# SPB Begreppslista

## Bidra

Du kan lägga till dina egna begrepp genom att skapa en markdownfil under mappen
innehall. Om du inte har ett gitlab konto så kan du skapa det här (det är kostnadsfritt).

### Namngivning

Ange begreppet som filnamn med datorvänliga tecken:

* a-z
* A-Z
* 0-9
* -_,

__Exempel__

anlaggningsmodell.md

### Mall  

Det finns en [mall](https://gitlab.com/swe-nrb/sbp-begreppslista/raw/master/underlag/mall.md) under mappen underlag.

### Steg för steg guide  

1. Kopiera mallen [LÄNK](https://gitlab.com/swe-nrb/sbp-begreppslista/raw/master/underlag/mall.md)
2. Skapa en ny fil under mappen underlag [LÄNK](https://gitlab.com/swe-nrb/sbp-begreppslista/new/master/innehall)
3. Benämna filen enligt ovan (ange begrepp som filnamn)
4. Spara som ett nytt ändringsförlag
