--------------------
__Förklaringsruta__  
Skriv endast ett begrepp per fil.
--------------------

## Begrepp  
--------------------
__Förklaringsruta__  
Ange endast en term under rubriken begrepp utan punkt eller andra tecken. Synonymer kan anges under rubriken synonymer
--------------------

Lorem ipsum  

### Beskrivning 

Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

### Källa (original)
--------------------
__Förklaringsruta__  
Ange som en lista med länkar
--------------------

* [SIS-infosida](http://www.sis.se)

### Synonymer
--------------------
__Förklaringsruta__
Ange synonymer som en lista
--------------------

* synonym 1
* synonym 2

### Plockat från (valfri)
--------------------
__Förklaringsruta__  
Detta fält skiljer sig från källa genom att begreppet förekommer i en publikation som i syn tur bygger på en originalkälla
--------------------

* [Google](http://www.google.com)

### Attributnamn (valfri)
--------------------
__Förklaringsruta__  
Skriv ett begrepp, Använd endast datorvänliga tecken såsom:
* a-z
* A-Z
* 0-9
* -_,
--------------------

lorem_ipsum
